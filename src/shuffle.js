export function shuffleToSolvable(arr) {
  let shuffled = shuffleSqr(arr);

  if (!isSolvable(shuffled)) {
    shuffled = swapLastTiles(shuffled);
  }

  return shuffled;
}

function shuffleSqr(arr) {
  const shuffledLine = shuffle([].concat(...arr));
  const shuffledSqr = [];
  for (let i = 0; i < 4; i++) {
    const startLine = i * 4;
    shuffledSqr.push(shuffledLine.slice(startLine, startLine + 4));
  }

  return shuffledSqr;
}

function shuffle(array) {
  let currentIndex = array.length, randomIndex;

  while (currentIndex > 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}

function isSolvable(arr) {
  const invCount = getInvCount([].concat(...arr));
  const nullPosition = getNullPosition(arr);

  return invCount % 2 === nullPosition % 2;
}

function getInvCount(arr) {
  let inv_count = 0;

  for (let i = 0; i < 15; i++) {
    for (let j = i + 1; j < 16; j++) {
      if (arr[j] && arr[i] && arr[i] > arr[j]) {
        inv_count++;
      }
    }
  }

  return inv_count;
}

function getNullPosition(arr) {
  for (let i = 0; i < 4; i++) {
    if (arr[i].findIndex(elem => elem === null) > -1) {
      return i + 1;
    }
  }
}

function swapLastTiles(arr) {
  let index1 = arr[3][3] ? 3 : 1;
  let index2 = arr[3][2] ? 2 : 1;

  [arr[3][index1], arr[3][index2]] = [arr[3][index2], arr[3][index1]];

  return arr;
}